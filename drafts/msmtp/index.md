+++
title = "Msmtp"
date = 2019-03-31
author = "pard68"
type = "post"
draft = true
+++

##############
## Defaults ##
##############

defaults
auth on
tls on
logfile ~/.msmtp/msmtp.log

################
## PROTONMAIL ##
################

## ipringle@protonmail.com
account protonmail
host 127.0.0.1
port 1025
#tls_starttls on
#tls_fingerprint B0:D0:19:97:F1:D0:5B:33:36:6A:D3:5E:45:3B:8D:D3:50:14:2F:FF:7C:E4:A7:CE:68:DF:07:EE:95:39:9A:27
tls_trust_file ~/.certs/protonmail.pem
from ipringle@protonmail.com
user ipringle@protonmail.com
password 698u1grxVGqMC1AldwVMQQ

## ipringle@pm.me
account pm
host 127.0.0.1
port 1025
#tls_starttls on
#tls_fingerprint B0:D0:19:97:F1:D0:5B:33:36:6A:D3:5E:45:3B:8D:D3:50:14:2F:FF:7C:E4:A7:CE:68:DF:07:EE:95:39:9A:27
tls_trust_file ~/.certs/protonmail.pem
from ipringle@pm.me
user ipringle@pm.me
password 698u1grxVGqMC1AldwVMQQ


## ian@dapringles.com
account dapringles
host 127.0.0.1
port 1025
#tls_starttls on
#tls_fingerprint B0:D0:19:97:F1:D0:5B:33:36:6A:D3:5E:45:3B:8D:D3:50:14:2F:FF:7C:E4:A7:CE:68:DF:07:EE:95:39:9A:27
tls_trust_file ~/.certs/protonmail.pem
from ian@dapringles.com
user ian@dapringles.com
password 698u1grxVGqMC1AldwVMQQ

## thisisfrom@pm.me
account thisisfrom
host 127.0.0.1
port 1025
#tls_starttls on
#tls_fingerprint B0:D0:19:97:F1:D0:5B:33:36:6A:D3:5E:45:3B:8D:D3:50:14:2F:FF:7C:E4:A7:CE:68:DF:07:EE:95:39:9A:27
tls_trust_file ~/.certs/protonmail.pem
from thisisfrom@pm.me
user thisisfrom@pm.me
password 698u1grxVGqMC1AldwVMQQ

################
## GANDI MAIL ##
################

## pard@0x44.pw
account 0x44
host mail.gandi.net
port 587
tls_starttls on
tls_fingerprint 01:B8:8C:E7:1D:34:8E:57:D8:28:AC:17:3D:F2:DE:4D:89:4C:A1:78:88:88:36:F5:48:0C:71:40:2F:C7:9D:69
tls_trust_file ~/.certs/gandi.pem
from pard@0x44.pw
user pard@0x44.pw
password OpsiBwn2acwtID#bb43.1
