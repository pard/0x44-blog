+++
title = "Rust Primitives"
date = 2019-02-01
author = "pard68"
type = "post"
draft = false
+++

Here is the src for a rust program I wrote that details primitives in Rust
(src is annotated):

```rust
fn main() {
	//type annotated variables
	let logical: bool = true;
	println!("boolean: {}", logical);
	
	//how many ways can I make an int?
	let a_int: i32 = 5;
	let also_int = 5i32;
	let still_int = 5;
	println!("these are all ints: {}, {}, {}",
	         a_int, also_int, still_int);
	
	//and if the variable is mutable than the type
	//can also be inferred automagically:
	let mut magic_int = 5;
	println!("i32: {}", magic_int);
	magic_int = 555555555555i64;
	println!("same var, but now i64! {}", magic_int);
	
	//other ints include:
	//i8, i16, i32, i64, i128, and isize
	//u8, u16, u32, u64, u128, and usize
	//signed and unsigned ints cannot be mixed!
	
	//integers can also be written like C-style 
	//hexidecimal, octal, and binary:
	let hex_int: i32 = 0x44;
	let oct_int: i32 = 0o104;
	let bin_int: i32 = 0b01000100;
	println!("hex: {}", hex_int);
	println!("oct: {}", oct_int);
	println!("bin: {}", bin_int);
	
	
	//and `_` can be used to make numbers C O M F Y:
	let comfy_int: i32 = 1_000_000;
	println!("comfy int: {}", comfy_int);
	
	//can't forget floats!
	let a_float: f64 = 5.0;
	let also_float = 5.0f64;
	let still_float = 5.0;
	println!("these are all floats: {}, {}, {}",
	         a_float, also_float, still_float);
	//there is f32 and f64 available
	
	//while a mutable variable can change values
	//this isn't Python and it cannot change type
	//still they can be over written:
	let ever_changing_var: i32 = 5;
	println!("i32: {}", ever_changing_var);
	let ever_changing_var: bool = true;
	println!("same var, now boolean: {}", 
	         ever_changing_var);
	
	//if () then tuple!
	let a_tuple = (5i32, 5u32, 5f32, 5f64, true, 'c');
	let a_tuple_of_tuples = ((4i32, false), (5i64, 6u64), 
	                         ('o', 'd'), (67.7f32));
	println!("My tuple: {:?}", a_tuple);
	println!("My nested tuple: {:?}", a_tuple_of_tuples);
	//the above two use `{:?}` to print with the debugger
	//because the standard print formatting cannot print
	//long tuples for $REASONS
	println!("The last tuple in my tuple of tuples: {}",
	         a_tuple_of_tuples.3);
	
	//tuples can be desconstructed like so:
	let constructed_tuple = (1i32, 2i32, 3i32, 4i32);
	let (a, b, c, d) = constructed_tuple;
	println!("deconstructed tuple: {}, {}, {}, {}",
	         a, b, c, d);
	
	//arrays are like tuples by all elements are the 
	//same type. An array uses [] instead of ()
	//an array's size is known at the time of compile
	//a slice is like an array with an unknown size at compile
	//and it actually two pointers, one to the data and the other
	//to the size of said data. on x64 architectures the 
	//pointer will be u64, on 32 bit systems it will be u32
	let an_array: [i32; 5] = [1, 2, 3, 4, 5];
	let a_slice = &an_array[1..4];
	println!("my array: {:?}", an_array); println!("my slice: {:?}",
	a_slice);
} 
```
