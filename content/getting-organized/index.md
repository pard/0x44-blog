+++
title = "Getting Organized"
date = 2019-01-21
author = "pard68"
type = "post"
draft = false
+++

I have put a reasonably amount of effort into getting myself organized. I am getting very 
tired of not recalling things even five or ten minutes later, and I am getting very sick of 
not remembering what it was that my wife wanted me to do. I tried org-mode but emacs is hard!
My current setup is:
 - vimwiki
 - taskwarrior
 - pomodoro
 - modifyed gtd

My work flow looks like this; everything goes into an `inbox.md` file. From there I decide if 
it is an idea or an actionable item. If it's an idea it goes into the vimwiki to be developed 
further (if needed). If it an actionable item it still goes into vimwiki, but it goes into my 
`todo.wiki` page, which uses the taskwiki plugin to talk to taskwarrior which manages my todo 
list for me. I can then use either taskwarrior or vimwiki to manage my todo list, depending 
on what I am trying to do. It also is nice because I can make very specific pages that are 
tied to my tasks. 

I am using pomodoro to track my time. Currently I am using `potato.sh` to track time, but I 
plan to write my own agent/deamon and track time via dmenu/i3blocks in the future. There is a 
npm package I found that does that, but npm... 

The modified gtd is really just a "I read the five points of gtd and then used that to make my
own workflow". I don't really know anything about gtd. I will get a more detailed post on my 
workflow when it is fleshed out a bit more and I get mermaid.js working...

