+++
title = "Oops v2"
date = 2019-08-26
author = "pard68"
type = "post"
draft = false
+++

It happened again.

I forgot to write anything.

Life happened again.

Since my last post I have changed to a day-shift and have had less time for things like blogging
and working on personal projects. But on the upside, I have been doing **much** better mentally/
emotionally and have been taking on far more responsibilities at work. I have automated a large
portion of our permissions management, specifically new hire permissions. I am also beginning
work on the opposite -- permission removal. I have also been working on Nagios monitoring more.

I hope that now things are starting to settle and my routine is more routine I can get back into
writing a post at least once a week and working on personal projects more consistently as well.

Here's hoping!
