+++
title = "An Open Recipe Format"
date = 2019-03-27
author = "pard68"
type = "post"
draft = false
+++

I have been toying with the idea of a recipe file format for a while. The main reason I was looking into this is because I have been playing around with a generic Python webscrapper to pull recipes from the plethora of ad and tracker ridden recipe websites and into a database. I use and reuse a lot of online recipes. The problem is I don't always know what recipe it was. So for a while I was printing them and adding them to a recipe box, but that only lasts so long. My thought was to create a database of all recipes I have used, and then I can go back and tweak them --use git or a git-like system to see what I changed-- add notes about what I did and didn't like, and most important, ensure that they will be there at all.

My first hurdle was to get to work on the scrapping. But that has proven difficult because while a cursory glance suggests most sites follow some form of standard recipe card format, the reality is that they do not. Not only that, I quickly went from Beautiful Soup to needing Selenium because a lot of stuff is loaded _ex post facto_ with JavaScript. So, I set the task aside.

And then today I started thinking about recipe file formats again and decided pursue the creation of a recipe file format, and eventually get back to the scrapping as well. The main reason I am going to pursue the file format is purely for learning. I think it would be a great learning opportunity to create a standard, even if it's only ever a standard used by me. 


[0]: http://microformats.org/wiki/recipe-formats

