+++
title = "March Wallpaper"
date = 2019-03-14
author = "pard68"
type = "post"
draft = false
+++

Today was a good day.

Last night I went to work and felt horrible. In a 48 hour time span I'd
managed 10 hours of sleep, five of those spent sleeping on my floor
because my bed was full of sick, puking people. Work was alright, minus
the coworker who sleeps from 9pm to 6:30am each day --aka the entire
shift. When I got home I crashed. Woke up around 3pm and I felt amazing.
And it got better, because we seem to have finally escaped that awful not
too cold but definitely not warm period and had a nice, 70 degree day. Got
to have a nice relaxing afternoon, made dinner, and drove to work with the
windows down and Wolves at the Gate keeping strangers at a comfortable
distance. Decided it's time for a new wallpaper, feast upon my ricing
goodness!

![rice-R-roni](./scrot.png "Turtle Wallaper")

### Featuring: 

*os*           | [Arch][5], [Linux][6] v. 5.0.1
---------------|--------
*dm*           | [i3-gaps][0] (not using the gaps features though)
*bar*          | [i3-blocks][1]
*visible apps* | [st][2], my fork is [here][3]
               | [neofetch][4]

[0]: https://github.com/Airblader/i3
[1]: https://github.com/vivien/i3blocks
[2]: http://suckless.org/st
[3]: https://github.com/pard68/st
[4]: https://github.com/dylanaraps/neofetch
[5]: https://www.archlinux.org/
[6]: https://www.kernel.org/
