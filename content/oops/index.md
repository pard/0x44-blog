+++
title = "Oops"
date = 2019-02-20
author = "pard68"
type = "post"
draft = false
+++

Life happened, and I haven't published any posts since the 2nd of Feb. Oops!

I am starting a new shift at work, kids have been sick, and school has kept me busy.

I will get back to Rust and to this blog. Currently working on a roke remote website 
running on flask, some SDR stuff with my new SDR USB dongle, and contemplating the idea 
of starting up a website for a resume service.

