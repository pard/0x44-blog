+++
title = "WebExtensions Are Fun"
date = 2019-03-14
author = "pard68"
type = "post"
draft = false
+++

I have been playing with WebExtensions lately. Mostly for a work project. The
goal is to get ticket data from Service Now and plug it into Nagios on a per
check/host basis. This way the history tab in Nagios will populate with
relevant ticket information -- namely if the ticket is still open. It's a small
thing but it took me a while to get running. Mostly because extensions are 3rd
class citizens. Just because you could do it on your system and just because
the website's own javascript can run it doesn't mean that an extension can run
it. 

The extension is made up of three main parts:

1. Find all valid tickets/tasks/problems/incidents in the history tab
2. Take these item numbers, call the Service Now REST API and get responses
3. Parse responses and put the relevant information back into the history tab.

Javascript is a pretty neat language. Dare I say it's even fun!

Troubleshooting is not so fun though. I have not found a reliable way to get
the extension to load every time. And for some reason my extension is currently
giving Nagios a nasty little twitch and I have no idea why. On top of that, the
responses from Service Now didn't work for a while. And by a while I mean it
took me a day to figure out that I a) needed to use CORS and b) needed to
specify special permissions for my extension to talk to the service-now.com
domain. One nice thing of CORS --nice because it makes me look like a way
better developer than I am-- is that it uses cookies for auth, a little native
pop-up appears, asks for the credentials, stores them as a cookie, and stops
bugging you from there on out. 

I have more plans for this app. I want to allow for mass acknowledgements from
the alert screen and not only from the 'Mass Acknowledge' page. This will be
very useful because the alert screen shows fewer alerts than the Mass
Acknowledge page and a lot of the alerts on the Mass Acknowledge page are
irrelevant. Also it will just make acknowledging similar alerts easier.

In other news, I made [a silly little extension][0] to see what publishing to
the official Firefox add-ons site is like. I don't have [m]any other plans for
this extension. Just wanted to get something up and running so I can check out
the dev side of the add-on store. Still, check it out, Google Maps will never
be the same after you install it.

[0]: https://addons.mozilla.org/en-US/firefox/addon/nsa-mode/
