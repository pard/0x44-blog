+++
title = "Rust Day 1"
date = 2019-02-01
author = "pard68"
type = "post"
draft = false
+++

I will be going through the official [Rust book][0] and supplementing with
["Rust by Example"][1], the [std lib doc][2], and potentially some of the
other [ official materials][3] if time and skill allows.

## Setup

In the introduction it mentions that Rust comes with its own install and
update management program called `rustup`. Chapter one starts where
most/all programming books start, installation. I will be installing Rust
as the book instructs (as opposed to using the pkg in pacman). I will
initially be installing it on my Lenovo T530 running Arch Linux. In the
future I may try using pacman to manage Rust and I will probably
experiment with running Rust on Windows as well, I'd even try it on macOS,
but I am not sure how I'd get my hands on a Mac so that probably won't be
happening...

Step one to installing Rust is getting `rustup`, which consists of
running:

`$ curl https://sh.rustup.rs -sSf | sh` (will not work without https)

This will download and then run a shell script. The script initially
prompts you and explains it will add a few locations to `PATH`. You have
the option of confirming, exiting, or customizing. I went with the default
options. The book mentions that you need a linker and a C compiler,
luckily `gcc` it pretty much a requirement to use Linux so I don't need to
bother getting a C compiler on my laptop --though this may be an important
point when it comes time to try Rust on Windows as I don't believe Windows
comes with a C compiler by default.

Updating Rust is as easy as `rustup update` and uninstalling is as `rustup
self uninstall`. I confirm Rust is installed by running `rustup
--version`, and lo and behold! I am running Rust version 1.16.0, which was
released on 18-12-06. 

The documentation mentions help can be found on their IRC channel (#rust
@ irc.mozilla.org) and their [forums][4]. I will make sure to get an
account setup on their forums at some point and to get their IRC channel
added as well. I also went ahead and subscribed to [r/rust][5]. As an
added bonus the Rust documentation is all available locally via `rustup
doc`, so I will still be able to work on this when Comcast inevitable
fails me once again!

## Hello World!

The Rust 'Hello World!' is all of three lines, no imports like in C: 

```rust
fn main() {
	println!("Hello, world!");
}
```

Sure enough, as soon as I get that into vim, save, and run `rustc main.rs`
I am greeted with a binary --the compiler spares no time for salutations
or statuses it's quick and to the point, like a systems programmer would
expect.

Anyone used to C and C derived languages will be familiar with the
standard `main(){}`. `println!` is not a function as I had suspected, but
a macro. The chapter mentions, in passing, the macros are denoted by the
`!`. 

## Cargo

Cargo is to Rust what pip is to Python -- a package manager, though
I suspect calling either merely a 'package manager' does neither justice.
Cargo can be used to start new Rust projects. It's actually really neat!
`cargo new $PROJECT_NAME` will create a new directory for that project,
create a `src` directory inside of that (preloaded with `main.rs`),
initial a git repo, and create a TOML file for configuration data
pertaining to the project.

```bash
total 24
drwxr-xr-x 4 pard68 users 4096 Feb  1 11:23 .
drwxr-xr-x 4 pard68 users 4096 Feb  1 11:23 ..
-rw-r--r-- 1 pard68 users  136 Feb  1 11:23 Cargo.toml
drwxr-xr-x 6 pard68 users 4096 Feb  1 11:23 .git
-rw-r--r-- 1 pard68 users   19 Feb  1 11:23 .gitignore
drwxr-xr-x 2 pard68 users 4096 Feb  1 11:23 src

./src:
total 12
drwxr-xr-x 2 pard68 users 4096 Feb  1 11:23 .
drwxr-xr-x 4 pard68 users 4096 Feb  1 11:23 ..
-rw-r--r-- 1 pard68 users   45 Feb  1 11:23 main.rs
```

The TOML file is super nice, it has the expected information; project
name, version, author, and something called "edition". But it also has
a section for dependencies, to my knowledge Python makes you maintain your
own `requirements.txt` file, so the fact that this maintains it for me is
really nice! (I am sure there is a way to automate this in Python too, but
it isn't a stock feature) I like this, when I am creating a new project
with Python and I want it to be organized nicely I always have to spend
five to ten minutes ~remembering~ searching the proper Python project
structure (by the way, there isn't an official one and there really should
be, so someone get PEP'ing!) 

You can use Cargo to build the project with `cargo build`. `cargo run`
will build and the execute and `cargo check` will check the code against
the compiler but won't actually compile it into an executable. When you
run the `cargo [build|run|check]` it adds a number of new files and
directories. I won't list them here, but the executable ends up in
`./taget/debug/`. And when it comes time to make a release version `cargo
build --release` will compile with optimizations and drop the executable in
`./target/release/`.

And that's all for chapter one folks! Tomorrow I will jump into chapter
two and get working on building a guessing game.

[0]: https://doc.rust-lang.org/book/ch01-01-installation.html 
[1]: https://doc.rust-lang.org/rust-by-example/index.html
[2]: https://doc.rust-lang.org/std/index.html
[3]: https://doc.rust-lang.org/
[4]: https://users.rust-lang.org/
[5]: https://www.reddit.com/r/rust
