+++
title = "Mutt"
date = 2019-03-31
author = "pard68"
type = "post"
draft = false
+++

I hate using a mouse, not because I am some neckbeard who refuses to admit Gopher died
before it was ever bootstrapped, but because using a mouse causes me a decent amount of
pain. So if I can keep my hands on a keyboard --ideally on the home row and with my
/comfy/ vim keybinds-- than I am happy. As a result, I have developed a love for the
terminal. One place I have struggled to get my workflow moved into the terminal, however,
is email. Mostly because I am lazy. I had it setup before but never used it enough to
really grok it.

So I have decided to revist the world of terminal email. After hours of DuckDuckGoing I
have not found a good guide that covers everything. I have found two guides ([1][0] &
[2][1]) which start down the road, but both stop after getting smtp setup and then the
authors never carry on, and since both posts are at least two years old I doubt that will
ever happen. So I am going to give this a shot and hopefully I won't get hit by a bus
before I can figure out, and then write a post about, the steps that follow imap and 
smtp setup.

For this guide we will be using the following software:

 * Proton Bridge
 * isync
 * msmtp
 * neomutt
 * neovim
 * mu
 * gpgme
 * w3m
 * urlscan
 * ripmime
 * vdirsyncer
 * khard

 I listed the above in the order that I will be covering them. For the sake of simplicity
 and keeping the length short-ish, each peice of software will be covered in its own
 post.

 Wish me luck, and hopefully I don't fall off the face of the earth before I can get to a
 topic beyond msmtp like those who have travelled this dangerous road before me!

 [0]: https://webgefrickel.de/blog/a-modern-mutt-setup
 [1]: https://bostonenginerd.com/posts/notmuch-of-a-mail-setup-part-1-mbsync-msmtp-and-systemd/
