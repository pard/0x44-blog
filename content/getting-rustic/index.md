+++
title = "Getting Rustic"
date = 2019-02-01
author = "pard68"
type = "post"
draft = false
+++

I am starting what will hopefully be a one month, self-imposed, self-taught intensive, deep dive into Rust. Why? Not entirely sure. Rust appeals to my desire to experience something that is new and unknown. Plus I like to fancy myself a systems programmer, or rather, I would like to be able to fancy myself as one and these seems like a good way to get there!

Plus, Rust is sort of awesome. It has a sick documentation and testing thing going for it. It is really safe, and it's really easy to make it safe, as in if it compiles than it' safe. And Rust has some really cool ways of interacting with other languages and Python in particular. Plus it's got low level speed and high level features, what's not to love!

Why a month, in this case 28 days? Because I don't think I can get it done if I don't impose a rigorous time frame upon myself. Life has a way getting busy really fast when you have children! If I don't stay on top of this that busy will inevitable find a way to sneak in and take away the time set aside for this venture. 

My code will ~all~ mostly be available on [github][0]. I will be going through the official Rust programming book, the Rust By Example, and maybe a few others. In addition I have begun to listen to the Rust podcast [New Rustacian][1], as I have found that listening to Python podcasts has helped me be a better coder, so why not with Rust too?

So without further ado... time to Rust!


[0]: https://github.com/learning2rust/
[1]: https://newrustacean.com/
