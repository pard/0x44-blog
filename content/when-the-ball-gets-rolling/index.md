+++
title = "When the Ball Gets Rolling"
date = 2019-01-13
author = "pard68"
type = "post"
draft = false
+++

Maybe it's just me, but I am slow to get going on new projects, but once I get into the, 
fix-it/do-it mode I just want to keep going. The other day I got my dryer down and took it 
apart to fix a squeak. Next thing I know I'm under the sink replacing the seals on my drain 
because they have been leaking. I get that done and I'm ready to get into the next project. 
But boy, oh boy I have such a hard time getting started doing anything at all. 

That sink has been leaking for a while (like literally a year at least). it's not that I 
don't _like_ fixing stuff, but for some reason I just don't want to get started. It's like 
going to the gym. I _know_ I will enjoy it, but I still don't want to put in the level of 
effort required to get to the enjoyment.
