+++
title = "Isync"
date = 2019-04-14
author = "pard68"
type = "post"
draft = false
+++

First step to getting [neo]mutt to work is to figure out how to
populate the maildir. This can be done in a few ways, including from
within mutt itself. The problem with using mutt is that mutt won't store
files for offline use and instead has to load the emails. This means two
things:

1. you need an internet connection

2. emails aren't backed up

There are ways around this, namely using a different tool to sync your
inboxes. A popular tool to use is `offlineimap`. However, I have _never_
been able to get it to work properly and currently it won't even run on my
computer. I imagine I could get it to work but, I don't really even care
to since `offlineimap` is running on Python 2.7 and the creator has already
stated that it will not be ported to Python 3, but will be replaced by
a new tool entirely. The other issue, for ProtonMail users, is that
`offlineimap` doesn't support the bridge software required to access
ProtonMail. 

Instead I have opted to use `isync`. `isync` is an older tool than
`offlineimap` but it works more or less the same way. You configure it
with a file in your `/home/` directory called `.mbsync`. Once configured
simply calling `mbsync -a` will sync all your inboxes and created any
required directories in your maildir. 

Here is a redacted version of my `.mbsync` configuration file:

```
##################
## pard@0x44.pw ##
##################

IMAPAccount 0x44
Host mail.gandi.net
Port 993
User USERNAME_HERE
Pass PASSWORD_HERE
#SSLType STARTTLS
#SSLVersions TLSv1.2
UseIMAPS yes
UseTLSv1 yes
CertificateFile ~/.certs/gandi.pem

IMAPStore 0x44-remote
Account 0x44

MaildirStore 0x44-local
Subfolders verbatim
Path ~/.mail/0x44/
Inbox ~/.mail/0x44/Inbox

Channel 0x44-inbox
Master :0x44-remote:
Slave :0x44-local:
Patterns * !"All Mail"
Create Both
Expunge Both
SyncState *

Group 0x44
Channel 0x44-inbox
```

Let's break this down a little to better understand:

```
IMAPAccount 0x44
Host mail.gandi.net
Port 993
User USERNAME_HERE
Pass PASSWORD_HERE
#SSLType STARTTLS
#SSLVersions TLSv1.2
UseIMAPS yes
UseTLSv1 yes
CertificateFile ~/.certs/gandi.pem
```

Here we use `IMAPAccount` to declare the name of the account, in this case `0x44`.
`Host` and `Port` correspond with the URL and port of the mailserver.
`User` and `Pass` correspond with the login and password for that 
inbox on the mailserver. I have `SSLType` and `SSLVersions` commented out
because my mailserver doesn't work with thosw enabled and have instead used
the out-of-date `UseTLSv1` which does seem to work. If you are using an email
hosted by Gandi.net you may need to use the settings I have above, otherwise
you should be able to just use the two commented out settings and the 
corresponding requirements for your mailserver.

`CertificateFile` is the hardest part in this section. I use not-as-normal
mailservers so I have to go and get them myself. For someone using Gmail, as
I understand it, you can just link to the default cert. folder on your machine
and it should be able to get the right one[^0]. But for me I had to get that file myself.
To do so I used the following command:

`openssl s_client -connect some.imap.server:port -showcerts`

The resulting output should contain a cert, which will start with a
`-----BEGIN CERTIFICATE-----` and end with a `-----END CERTIFICATE-----`.
You want all of that cert, including the line with `BEGIN` and the one with `END`.
If there are multiple certificates you will probably want the first of the two certs.
Take that information and pop it into a file in `~`, I elected to use `~/.certs/`.

```
IMAPStore 0x44-remote
Account 0x44
```

Here we set up the variable `0x44-remote` which will be used from here on out to
reference the remote IMAPS location for my inbox. And likewise `0x44` will reference
the name of the account in question.

```
MaildirStore 0x44-local
Subfolders verbatim
Path ~/.mail/0x44/
Inbox ~/.mail/0x44/Inbox
```

You more or less will want the above verbatum, of course just making it unique to
your inbox. A `s/0x44/my-inbox-name/g` will make most of this config (minus the account stuff)
read to go for you. You may wish to change the maildir. I opted for `.mail`. However
I think that the standard is just `Mail`.

```
Channel 0x44-inbox
Master :0x44-remote:
Slave :0x44-local:
Patterns * !"All Mail"
Create Both
Expunge Both
SyncState *
```

So this is a little different than other isync tutorials you will find.
Most will have you add each folder (INBOX, Draft, Sent, Spam, Trash, etc.),
but we are using the `Patterns` to select ALL folders in my inbox EXCEPT for
the "All Mail" folder. `Create Both` and `Expunge Both` will keep your
local and remote IMAPS directories the same. I like that, some people may not,
but I do!

```
Group 0x44
Channel 0x44-inbox
```

For our purposes these two lines seem almost pointless. However,
for most of the other tutorials I mentioned above this is an important
portion of the config. In theory this ties all the folders together into
a single group so that they are all sync'd together. Of course we only have
a single `Channel` and thus don't really have much to put into this portion.

That's it. If all went well you can now `mbsync -a` and all added mail accounts
will begin to sync. It will yell at you if you don't make the initial parent
directory for each account, in my case `~/.mail/0x44/` but it will handle all
the child directories within that. 

If things don't work right try again with `--verbose` or `-V` and if that isn't
enough than try `--debug` or `-D`.

If you are using ProtonMail or Gandi.net and are having troubles let me know
by sending me an email (pard@0x44.pw) and I will try to help you troubleshoot!

[^0]: On Arch Linux they are found in `CertificateFile /etc/ssl/certs/ca-certificates.crt`

