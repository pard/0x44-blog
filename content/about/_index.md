+++
title =  "about"
template = "about.html"
+++

I am a systems administrator. I get paid to work with both Linux and Windows servers.
Outside of work I work with Linux machines exclusively. My go-to language is Python
-- however Rust is quickly surpassing Python as my language of choice these days.
I am also comfortable in C, Go, Bash, and PowerShell. If you are interested in seeing
a resume, feel free to email me.

I have some animals: a rabbit, two dogs, three horses, three turtles (an Eastern Paint, a yellow
belly slider, and a African sideneck; all are rescues), ten chickens, two ducks, three cats,
a bunch-o-fish, four lizards (beared dragon and three blue tailed skink), a cornsnake,
and a boa constrictor.

I like to tinker and build. I am currently working on a project I call RECS which stands for Reptile
Environmental Control System. This is a single microcontroller to manage lighter, heat,
and various atomospheric conditions in reptile habitats. I am also fascinated by low-tech aquariums.
I am currently working on tranistioning my 150 gallon aquarium's filtration to an overhead
refugium/planter. So far my record is three months with good numbers and no water changes. This was
when the tank had a turtle, an oscar, three African cichlids, and two common plecos.

I have a degree in New Testament studies, with a minor in Koine -- everything I know about computers and
computer science I have taught myself.

I am opinionated.
