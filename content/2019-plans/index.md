+++
title = "2019 Plans"
date = 2019-01-09
author = "pard68"
type = "post"
draft = false
+++

I have a few plans for 2019. I'd like to get my pond installed and enclosed so Leo can move
outside and the 150g can be used to raise up some Argetinian side necks. Eventually he will
probably come back inside as a permanent fixture and the side necks will move out. On the topic 
of the 150g, I want to get a new filter setup for it this year.

We are planning on taking in two rescued Sulcatas so that will be a very big part of 2019++. 
Probably keep them inside this year, at least, while we figure out a proper outdoor enclosure.

I would also like to get a bearded dragon this year. I eventually would like an Argetine Tegu, 
and a beardy would be good practice. 

On the house front, I want to get the living room walls repaired and painted. The roof *needs* 
to be repaired. There are a few plumbing jobs I have to tackle this quarter as well. 

On the job development front, I really need to start using my Linux Academy subscription and 
knock out a few courses/certs. I should get my REHL cert. since it's on work's dime. I also 
would like to get a better grip on Go and learn Rust. 

In terms of new year goals, I want to make blogging a regular habit. I think it is healthy, 
basically a journal. I would like to get back to my pre-wedding weight and activity level. I 
guess that is healthy as well.

