+++
title = "DIY"
date = 2019-01-10
author = "pard68"
type = "post"
draft = false
+++

I think I have always been inclined towards doing things for myself. That has never been more true than since I have
purchased my own house. Apart from some roof repairs and the one time I broke down and called an HVAC guy, I have been 
able to take care of all my own home repairs. It's been reazlly rewarding! Learn a lot, save a lot, and it's so nice 
to know that you fixed it, and can fix future issues. I am thinking about this now because I have a very squeaky 
dryer that I need to take apart and de-squeak this weekend. Luckily it seems that mmost sdryer squeaks are easily and 
freely fixed. I also have a kitchen sink with some leaky drain seals, a front yard that needs some drainage added 
in order to stop the river that cuts through my basement after every big rain, a janky-redneck fence to finish, 
and a few other projects to tackle. 
