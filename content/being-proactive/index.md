+++
title = "Being Proactive"
date = 2019-01-14
author = "pard68"
type = "post"
draft = false
+++

Being proactive is hard. 

I enrolled in Linux Academy back in October because they had 
a sale on the cost of admission and I had just taken a job 
with System Operations so I felt I should celebrate. Since 
then I have not completed any courses at all. Life gets in 
the way. Excuses are easy to come by and cheap to use. I 
really should be making use of this resource because a) I 
paid for it and b) it will help me to move forward in my 
career path of choice. And still, I am struggling to just do 
it. 

I know I could probably find some trendy plan, or notebook, 
or app to revolutionize my life and make me feel fulfilled, 
while also increasing productivity. But those things don't 
really do anything special. They are just like the New Year 
gym population spikes. People are pumped. But keeping the 
momentum is where we all fail.

I've been in a really DIY mood lately. I think it's time I 
just bite that proverbial bullet, start working on my 
classes (in addition to the actual college classes) and 
hope that eventually it will become a habit.

