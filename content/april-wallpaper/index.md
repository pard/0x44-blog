+++
title = "April Wallpaper"
date = 2019-03-30
author = "pard68"
type = "post"
draft = false
+++

I figured I'd go for something tongue-in-cheek for April. You may not know it
yet but Microsoft is now a Linux fanboy too and poised to take the Linux world
by ~force~ storm. So I figured I'd avoid the beatings and jump on the
bandwagon now. 

Enjoy!

![rice-R-roni](./scrot.png "In Honor of Our New Overlords")

### Featuring: 

*os*           | [Arch][5], [Linux][6] v. 5.0.5
---------------|--------
*dm*           | [i3-gaps][0] (not using the gaps features though)
*bar*          | [i3-blocks][1]
*visible apps* | [st][2], my fork is [here][3]
               | [neofetch][4]

If you want to get your hands on my gruvbox colored Windows 95 wallpaper,
[help yourself][7]!

[0]: https://github.com/Airblader/i3
[1]: https://github.com/vivien/i3blocks
[2]: http://suckless.org/st
[3]: https://github.com/pard68/st
[4]: https://github.com/dylanaraps/neofetch
[5]: https://www.archlinux.org/
[6]: https://www.kernel.org/
[7]: ./win95gruvbox.png
