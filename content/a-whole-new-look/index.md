+++
title = "A Whole New Look"
date = 2019-03-02
author = "pard68"
type = "post"
draft = false
+++

You may notice that the site has changed. My prior theme was cool and whatever, but it had
some major problems. Namely, the site wasn't ideal on phones, by that I mean it SUCKED!
Also, HTML/CSS is hard so I decided to not bother trying to figure out how to make it all 
work. I found this sweet theme and plan to give it a run, JS and all!

