+++
title = "a new look"
date = 2019-09-22
author = "pard68"
type = "post"
draft = false
+++

Got bored at work and started playing with HTML and CSS. Ended up with what you see here.
It's still a work in progress, but it's more in line with what I want my site to look like.
It's clean, very minimal, no js, and it's functional. Get's to the point and does what I need.
Ended up opting for a new SSG at the same time. I initially thought to write my own, and I still
might. But for the time being I am using the Rust SSG called [Zola][0]. Few reasons:

1. It's minimal
1. It's Rust, so I can contribute
1. It's using a very powerful template engine called [Tera][1]

If I decide to write an SSG I will end up using Tera for an engine. The syntax is like Jinga2,
except with Rust flavoring. I might do a write up on how to create a Zola theme, because I found
the documentation to be lacking, which means others will probably find it lacking as well.

[0]: https://www.getzola.org/
[1]: https://tera.netlify.com/
