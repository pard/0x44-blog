+++
title = "A New Home"
date = 2019-01-09
author = "pard68"
type = "post"
draft = false
+++

0x44.pw is the new home of wannabedev.net. 

No real reason why, except I wanted a place to write about more than just computers and it felt like a good way to
start the new year. Some of the changes include moving to Hugo from Jekyll. I made this decision because `Go > Ruby`,
and Hugo provides a much more intuitive, to me, means of producing content. I am currently in the process of 
designing my own theme for this site based off the default [werc][0] style, which I would have used if I could have 
figured it out...

Old wannabedev.net content and new content in the same genre will now be located at [dev.0x44.pw][1]. This subdomain,
www, will be for more generic life stuff. I also will have content at [herps.0x44.pw][2] pertaining to reptiles, 
reptile keeping, and other related content (probably keep fish stuff there as well). [docs.0x44.pw][3] will 
eventually be the home of my wiki and it, along with [git.0x44.pw][4] will probably remain empty for a while until
I get everything else setup.


[0]: http://werc.cat-v.org/
[1]: https://dev.0x44.pw/
[2]: https://herps.0x44.pw/
[3]: https://docs.0x44.pw/
[4]: https://git.0x44.pw/
